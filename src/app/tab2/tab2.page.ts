import { Component, OnInit } from '@angular/core';
import { WeatherService } from '../services/weather.service';
import * as moment from 'moment';

@Component({
  selector: 'app-tab2',
  templateUrl: 'tab2.page.html',
  styleUrls: ['tab2.page.scss']
})
export class Tab2Page implements OnInit {

  public forecastWeather: any;

  constructor(private weatherService: WeatherService) {}

  ngOnInit() {
    this.weatherService.getForecastWeather()
      .then((data: any) => {
        if ((data.list) && (data.list.length > 0)) {
          this.forecastWeather = data.list.map((forecast: any) => {
            return {
              main: forecast.main,
              weather: forecast.weather,
              dt_txt: forecast.dt_txt
            };
          });
        }
      });
  }

  getLowTemperature(forecast: any) {
    return Math.round(forecast.main.temp_min);
  }

  getHighTemperature(forecast: any) {
    return Math.round(forecast.main.temp_max);
  }

  getWeatherImage(forecast: any) {
    return this.weatherService.getWeatherImage(forecast.weather[0].id);
  }

  getWeatherTitle(forecast: any) {
    return this.weatherService.getWeatherTitle(forecast.weather[0].id);
  }

  getWeatherDate(forecast: any) {
    const day = moment(forecast.dt_txt, 'YYYY-MM-DD HH:mm:ss');
    return `${day.format('ddd')} ${day.format('hh:mm a')}, ${day.format('MMM')} ${day.format('DD')}, ${day.format('YYYY')}`;
  }

}
