import { Component } from '@angular/core';

@Component({
  selector: 'app-tabs',
  templateUrl: 'tabs.page.html',
  styleUrls: ['tabs.page.scss']
})
export class TabsPage {

  public currentTab: string;

  constructor() {}

  onSelected(event) {
    this.currentTab = event.tab;
}

}
