import { Component, OnInit } from '@angular/core';
import { WeatherService } from '../services/weather.service';
import { Geolocation } from '@ionic-native/geolocation/ngx';

@Component({
  selector: 'app-tab1',
  templateUrl: 'tab1.page.html',
  styleUrls: ['tab1.page.scss']
})
export class Tab1Page implements OnInit {

  public temperature: number;
  public weatherImage: string;
  public weatherTitle: string;
  public latitude: number;
  public longitude: number;
  public country: string;
  public city: string;

  constructor(private weatherService: WeatherService) {
  }

  ngOnInit() {
    this.weatherService.getCurrentWeather()
      .then((data: any) => {
        if (data.sys.country) {
          this.country = data.sys.country;
        }
        if (data.name) {
          this.city = data.name;
        }
        if (data.main.temp) {
          this.temperature = Math.round(data.main.temp);
          if (data.weather[0].id) {
            this.weatherImage = this.weatherService.getWeatherImage(data.weather[0].id);
            this.weatherTitle = this.weatherService.getWeatherTitle(data.weather[0].id);
          }
        }
      });
  }

}
