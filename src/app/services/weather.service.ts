import { Injectable } from '@angular/core';
import { environment } from '../../environments/environment';
import { HttpClient } from '@angular/common/http';
import { HTTP } from '@ionic-native/http/ngx';
import { Geolocation, Geoposition } from '@ionic-native/geolocation/ngx';
import { Platform } from '@ionic/angular';

@Injectable({
  providedIn: 'root'
})
export class WeatherService {

  private apiUrl: string;
  private location: Geoposition;
  private weatherConfig;
  private weatherImage: string;
  private weatherTitle: string;
  public latitude: number;
  public longitude: number;

  constructor(
    private geolocation: Geolocation,
    private httpClient: HttpClient,
    private http: HTTP,
    public platform: Platform) {
    this.weatherConfig = [
      {
        weatherCodes: [800],
        weatherImage: 'sunny',
        weatherTitle: 'Sunny'
      },
      {
        weatherCodes: [741],
        weatherImage: 'fog',
        weatherTitle: 'Fog'
      },
      {
        weatherCodes: [801],
        weatherImage: 'cloudy',
        weatherTitle: 'Cloudy'
      },
      {
        weatherCodes: [802, 803, 804],
        weatherImage: 'overcast',
        weatherTitle: 'Overcast'
      },
      {
        weatherCodes: [300, 301, 302, 310, 311, 312, 313, 314, 321],
        weatherImage: 'light-rain',
        weatherTitle: 'Light Rain'
      },
      {
        weatherCodes: [500, 501, 502, 503, 504, 511, 520, 521, 522, 531],
        weatherImage: 'shower',
        weatherTitle: 'Shower'
      },
      {
        weatherCodes: [200, 201, 202, 210, 211, 212, 221, 230, 231, 232],
        weatherImage: 'tstorm',
        weatherTitle: 'Thunderstorm'
      },
      {
        weatherCodes: [600, 601, 602, 611, 612, 613, 615, 616, 620, 621, 622],
        weatherImage: 'snow',
        weatherTitle: 'Snow'
      }
    ];
  }

  async getCurrentWeatherApi() {
      this.location = await this.geolocation.getCurrentPosition();
      this.apiUrl = environment.openWeatherURL
      + environment.openWeatherCurrent
      + '?lat=' +  this.location.coords.latitude
      + '&lon=' + this.location.coords.longitude
      + environment.openWeatherQuery
      + environment.openWeatherKey;
  }

  async getForecastWeatherApi() {
    this.location = await this.geolocation.getCurrentPosition();
    this.apiUrl = environment.openWeatherURL
    + environment.openWeatherForecast
    + '?lat=' +  this.location.coords.latitude
    + '&lon=' + this.location.coords.longitude
    + environment.openWeatherQuery
    + environment.openWeatherKey;
}

  async getCurrentWeather() {
    console.log(this.platform.platforms());
    let servicePromise: any;
    await this.getCurrentWeatherApi();
    if (this.platform.is('android') || this.platform.is('ios')) {
      servicePromise = await this.http.get(this.apiUrl, {}, {});
    } else {
      servicePromise =  await this.httpClient.get(this.apiUrl).toPromise();
    }
    return servicePromise;
  }

  async getForecastWeather() {
    await this.getForecastWeatherApi();
    return await this.httpClient.get(this.apiUrl).toPromise();
  }

  getWeatherImage(weatherCode: number) {
    this.weatherImage = this.weatherConfig
                        .filter((weatherItem) => {
                          return weatherItem.weatherCodes.includes(weatherCode);
                        })
                        .map((weatherItem) => {
                          return '../../assets/' + weatherItem.weatherImage + '.png';
                        });
    if (!this.weatherImage) {
      this.weatherImage = '../../assets/dunno.png';
    }
    return this.weatherImage;
  }

  getWeatherTitle(weatherCode: number) {
    this.weatherTitle = this.weatherConfig
                        .filter((weatherItem) => {
                          return weatherItem.weatherCodes.includes(weatherCode);
                        })
                        .map((weatherItem) => {
                          return weatherItem.weatherTitle;
                        });
    if (!this.weatherTitle) {
      this.weatherTitle = 'Dunno';
    }
    return this.weatherTitle;
  }

}
