import { TestBed } from '@angular/core/testing';

import { WeatherService } from './weather.service';
import { Geolocation } from '@ionic-native/geolocation/ngx';
import { HttpClientTestingModule } from '@angular/common/http/testing';
import { HTTP } from '@ionic-native/http/ngx';
import { Platform } from '@ionic/angular';

describe('WeatherService', () => {
  beforeEach(() => TestBed.configureTestingModule({
    imports: [HttpClientTestingModule],
    providers: [Geolocation, HTTP, Platform]
  }));

  it('should be created', () => {
    const service: WeatherService = TestBed.get(WeatherService);
    expect(service).toBeTruthy();
  });

  it('should get current weather ', () => {
    const service: WeatherService = TestBed.get(WeatherService);
    service.getCurrentWeather()
    .then((data: any) => {
      expect(data.main.temp).toBeTruthy();
    });
  });
});
