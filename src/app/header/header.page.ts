import { Component, OnInit, Input } from '@angular/core';
import { IonicAngularThemeSwitchService } from 'ionic-angular-theme-switch';

@Component({
  selector: 'app-header',
  templateUrl: './header.page.html',
  styleUrls: ['./header.page.scss'],
})
export class HeaderPage implements OnInit {
  @Input() tabTitle: string;
  public title: string;
  public isToggleChecked: boolean;
  private darkConfig = {
    primary: '#549ee7',
    secondary: '#5fb3b3',
    tertiary: '#fac863',
    success: '#90d089',
    warning: '#f99157',
    danger: '#ec5f67',
    light: '#d8dee9',
    medium: '#65737e',
    dark: '#1b2b34',
    'ion-background-color': '#1b2b34',
    'ion-text-color': '#fff'
  };

  constructor(
    private themeSwitchService: IonicAngularThemeSwitchService
  ) { }

  ngOnInit() {
    this.themeSwitchService.getStoredThemeName().then(
      (storedThemeName) => {
        if ((storedThemeName) && (storedThemeName === 'dark')) {
          this.isToggleChecked = true;
        }
      }
    );
  }

  setTitle() {
    if (this.tabTitle === 'current') {
      this.title = 'Current Weather';
    } else if (this.tabTitle === 'forecast') {
      this.title = 'Forecast';
    }
    return this.title;
  }

  onToggleChange(event: { detail: { checked: any; }; }) {
    this.isToggleChecked = event.detail.checked;
    if (this.isToggleChecked) {
      this.setDarkMode();
    } else {
      this.setLightMode();
    }
  }

  setDarkMode() {
    this.themeSwitchService.setTheme(this.darkConfig, 'dark');
  }

  setLightMode() {
    this.themeSwitchService.setTheme();
  }

}
