import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { IonicModule } from '@ionic/angular';
import { IonicAngularThemeSwitchService } from 'ionic-angular-theme-switch';

import { HeaderPage } from './header.page';
import { IonicStorageModule } from '@ionic/storage';
import { DebugElement } from '@angular/core';

describe('HeaderPage', () => {
  let component: HeaderPage;
  let fixture: ComponentFixture<HeaderPage>;
  let themeSwitchService: IonicAngularThemeSwitchService;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ HeaderPage ],
      imports: [IonicModule.forRoot(), IonicStorageModule.forRoot()],
      providers: [IonicAngularThemeSwitchService]
    }).compileComponents();

    themeSwitchService = TestBed.get(IonicAngularThemeSwitchService);

    fixture = TestBed.createComponent(HeaderPage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  }));

  it('should create', () => {
    expect(component).toBeTruthy();
  });

  it('should have header title ', () => {
    component.tabTitle = 'current';
    expect(component.setTitle().length).toBeGreaterThan(0);
  });

  it('should set dark mode ', async () => {
    component.setDarkMode();
    const storedThemeName = await themeSwitchService.getStoredThemeName();
    expect(storedThemeName).toEqual('dark');
  });

});
