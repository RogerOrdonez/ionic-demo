# Angular Dev Assessment
This is my personal repository for Angular Dev Assessment (NicaSource)

## Clone the repository
Create a new folder in your file system, switch to your new folder and open a new terminal, then execute the next command: 

    git clone https://RogerOrdonez@bitbucket.org/RogerOrdonez/ionic-demo.git

## Install dependencies and run the project
Execute the next command for install the project dependencies

    npm install

When the installation is finished, execute the next command for compiling:

    ionic serve

Once the compilation is complete, if the project starts correctly, you should be able to access *http://localhost:8100* from your browser and be able to view the weather application.    